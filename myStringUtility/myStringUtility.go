package myStringUtility

func Reverse(s string) string {
	b := []rune(s)   			// the "rune" type can handle unicode characters properly, while "byte" would fail
	for i:=0; i < len(b)/2; i++ {
		j := len(b)-i-1
		b[i], b[j] = b[j], b[i];
	}
	return string(b)
}
